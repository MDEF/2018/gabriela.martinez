---
title: 12 • Output Devices
period: 03-10 April 2019
date: 2019-04-10 12:00:00
term: 2
published: true
---


<br>
<br>
Assignment: Replicate a Neil’s board and program it. - work in pairs
<br>
<br>
For this week assignment I worked with Barbara Drozdek. As explained in the input devices week, we wanted to start working in parts for making a pottery wheel - a now postponed project. But as a way to try out related processes, we decided as an output device to be fabricated related to that project, so we did the DC motor board. For more information about DC Motors, Fab Lab BCN shared with us in the class a page with the [basics of DC Motors](https://itp.nyu.edu/physcomp/lessons/dc-motors/dc-motors-the-basics/).
<br>
<br>
From this [week’s class page](http://academy.cba.mit.edu/classes/output_devices/index.html) we could get all the information for making the *hello.H-bridge.44* board, the one able to control a DC motor. We downloaded the images for the traces and the outcut line, processed it on fabmodules with the following parameters:
<br>
<br>
{% figure caption: "*Fabmodules - traces parameters*" %}
![]({{site.baseurl}}/fa_w12_01.jpg)
{% endfigure %}
{% figure caption: "*Fabmodules - outcut parameters*" %}
![]({{site.baseurl}}/fa_w12_02.jpg)
{% endfigure %}
{% figure caption: "*Milling process*" %}
![]({{site.baseurl}}/fa_w12_03.jpg)
{% endfigure %}
<br>
<br>
After cutting the board, it was time to solder the components.Barbara soldered it. For the  *hello.H-bridge.44* board it is needed:
<br>
01  Attiny 44
<br>
02 capacitor 1uF (c1/c2)
<br>
01 resistor 10k (r1)
<br>
01 connector ISP 6-pin
<br>
01 4-pin connector(j2)
<br>
01 A4953 - H-bridge motor driver
<br>
01 capacitor 10uF
<br>
01 4-pin connector (j3)
<br>
01 regulator IC2 - 5V
<br>
<br>
With the board done, I checked the voltage flow in the board with multimeter, everything was fine.
<br>
<br>
{% figure caption: "*Checking with multimeter*" %}
![]({{site.baseurl}}/fa_w12_04.jpg)
{% endfigure %}
<br>
<br>
For programming the board, Barbara connected it to the FabISP, with attention to the way both connect - with correct vcc and ground orientation. The process to program it was done with Arduino. To program the board, we connect it to the computer and to a 9V  battery. Then, do burn bootloader quickly to not burn the output board. After this step, the board should be disconnected from one of the sources, the battery or the computer. With the board successfully programmed, we could control the DC motor.
<br>
<br>
{% figure caption: "*Testing the board*" %}
![]({{site.baseurl}}/fa_w12_05.jpg)
{% endfigure %}
<br>
<br>
**Download the files**
<br>
<br>
[hello.H-bridge.44.traces.png]({{site.baseurl}}/hello.H-bridge.44.traces.png)
<br>
[hello.H-bridge.44.interior.png]({{site.baseurl}}/hello.H-bridge.44.interior.png)
<br>
[hello.H-bridge.44.DC.make]({{site.baseurl}}/hello.H-bridge.44.DC.make)
<br>
[Arduino_DC-motor_Blink1.ino]({{site.baseurl}}/Arduino_DC-motor_Blink1.ino)
