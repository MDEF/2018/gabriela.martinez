
Gabi@DESKTOP-I5C1KB9 MINGW64 ~/Documents/MDEF/02_term/Fab Academy/week-11/files
$ make -f hello.mag.45.make
avr-objcopy -O ihex hello.mag.45.out hello.mag.45.c.hex;\
        avr-size --mcu=attiny45 --format=avr hello.mag.45.out
AVR Memory Usage
----------------
Device: attiny45

Program:     510 bytes (12.5% Full)
(.text + .data + .bootloader)

Data:          0 bytes (0.0% Full)
(.data + .bss + .noinit)



Gabi@DESKTOP-I5C1KB9 MINGW64 ~/Documents/MDEF/02_term/Fab Academy/week-11/files
$ make -f hello.mag.45.make program-usbtiny
avr-objcopy -O ihex hello.mag.45.out hello.mag.45.c.hex;\
        avr-size --mcu=attiny45 --format=avr hello.mag.45.out
AVR Memory Usage
----------------
Device: attiny45

Program:     510 bytes (12.5% Full)
(.text + .data + .bootloader)

Data:          0 bytes (0.0% Full)
(.data + .bss + .noinit)


avrdude -p t45 -P usb -c usbtiny -U flash:w:hello.mag.45.c.hex

avrdude: initialization failed, rc=-1
         Double check connections and try again, or use -F to override
         this check.


avrdude done.  Thank you.

make: *** [program-usbtiny] Error 1

Gabi@DESKTOP-I5C1KB9 MINGW64 ~/Documents/MDEF/02_term/Fab Academy/week-11/files
$
