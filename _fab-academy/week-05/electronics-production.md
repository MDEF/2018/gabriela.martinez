---
title: 05 • Electronics Production
period: 13-20 February 2019
date: 2019-02-20 12:00:00
term: 2
published: true
---


<br>
<br>
Assignment: make an in-circuit programmer by milling the PCB, soldering the components and programming it
<br>
<br>
I was not familiar with electronics production, so all in this week was really new to me. By that [classes given by FAB LAB Barcelona tutors](http://fabacademy.org/2019/labs/barcelona/local/wa/week4/) were very much of instruction to me.
<br>
<br>
To start the assignment, the first task was to mill a PCB board. The one I milled was the *Hello ISP 44*. We had the images of the board in png formats, two different archives, one for the [inner traces](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.traces.png) (to be milled with 1/64” end mill) and one for the [outside cut](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.interior.png) (to be milled with 1/32” end mill). The png files were transformed in g-code using [MODS](http://mods.cba.mit.edu/). With the files ready, first I milled the inner traces and then later the outside cut.
<br>
<br>
{% figure caption: "*MODS interface*" %}
![]({{site.baseurl}}/fa_w05_01.jpg)
{% endfigure %}
<br>
{% figure caption: "*Milling process*" %}
![]({{site.baseurl}}/fa_w05_02.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Result of the milling process*" %}
![]({{site.baseurl}}/fa_w05_03.jpg)
{% endfigure %}
<br>
<br>
For soldering the [components](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png), I first checked the [list of required components](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html) and organized then in my notebook. The Fab Lab was missing the mini usb connector, so that is why my board initially does not have it.
<br>
<br>
{% figure caption: "*List of components to be soldered*" %}
![]({{site.baseurl}}/fa_w05_04.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*PCB in soldering process and PCB almost done*" %}
![]({{site.baseurl}}/fa_w05_05.jpg)
{% endfigure %}
After soldering the mini usb that was missing, I checked if the board was conducting the information right with a multimeter. Then I proceeded with the programming of the board. I started following the tutorial available in the [Fab Academy website](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html).
<br>
<br>
{% figure caption: "*Board with all components soldered*" %}
![]({{site.baseurl}}/fa_w05_06.jpg)
{% endfigure %}
<br>
<br>
I connected the board in the computer and could see it was the board was receiving power correctly and everything was solder properly.
<br>
<br>
{% figure caption: "*Board receiving power*" %}
![]({{site.baseurl}}/fa_w05_07.jpg)
{% endfigure %}
<br>
<br>
I have a Windows 10 and installed the corresponding the AVR in [this version](https://sourceforge.net/projects/winavr/files/WinAVR/20100110/). Later I installed the firmware available in the [class page](http://academy.cba.mit.edu/classes/electronics_production/index.html) as a zip file. I unzipped the file and with the board connected, proceeded to program the board. I opened Gitbash and proceeded with the commands: *make clean*, *make hex* and *make fuse*. With the *make fuse*, I got an error as indicated in the image below.
<br>
<br>
{% figure caption: "*Error while installing firmware*" %}
![]({{site.baseurl}}/fa_w05_08.jpg)
{% endfigure %}
<br>
<br>
Then, a colleague from Fab Academy commented he had the same problem and gave me new files from the AVRDUDE, which would work on Windows 10. (Thank you, Josep!). The files were replaced in “c:> WinAVR-20100110 > bin ”. I tried to proceed with the installation but later got a different error while programming it. So tutor Santi programmed my board in his Mac, and it was properly installed.
<br>
<br>
[w05_fabisp_programm_process - txt file]({{site.baseurl}}/w05_fabisp_programm_process.txt)
<br>
<br>
After being programmed, my computer could identify the board but couldn’t find drivers for it. So, with the Zadig program, indicated in [this tutorial](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/windows_avr.html) as a tool to install drivers for the programmer board, I could install the corresponding driver to identify the Hello ISP 44.
<br>
<br>
{% figure caption: "*Drivers installed in the Hello ISP 44*" %}
![]({{site.baseurl}}/fa_w05_09.jpg)
{% endfigure %}
<br>
<br>
After doing all this, I unsoldered the 0 resistors. The board now will be used during Fab Academy for other purposes of programming.
<br>
<br>
{% figure caption: "*Hello ISP 44 final configuration*" %}
![]({{site.baseurl}}/fa_w05_10.jpg)
{% endfigure %}
<br>
<br>
