---
title: 01 • Introduction
period: 16-22 January 2019
date: 2019-01-16 12:00:00
term: 2
published: true
---


 <br>
 <br>
### What is Fab Academy?
 <br>
 <br>
 Fab Academy is a distributed educational model providing a unique educational experience.
 <br>
 <br>
Each Fab Lab that participates in the Fab Academy program is part of a global Fab Lab / Fab Academy network. These Fab Labs are Nodes that offer the Fab Academy program.
<br>
<br>
Students view and participate in global lectures broadcasted every Wednesdays at 9:00 am – 12:00 pm EST. The lectures are recorded and available to students throughout the semester. In addition to the lectures, there are 2 / 3 lab days each week where students have access the digital fabrication equipment and personal help with projects. Each Fab Lab will establish the schedule for these Lab days.
<br>
<br>
Fab Academy faculty, who are leaders in their respective fields, provide global video lectures, supervise academic content, and guide research. Hands-on instruction in the labs is provided by instructors who supervise and evaluate Certificates, develop and disseminate instructional material, and assist with projects.
<br>
<br>
Source: http://fabacademy.org/about/#how-it-works
<br>
<br>
### Who am I?
<br>
<br>
I graduated in Architecture and Urbanism from Universidade de São Paulo (FAUUSP, 2012) with an exchange program at Universidad Politécnica de Madrid (ETSAM-UPM). I collaborated on academic projects at LABAUT FAUUSP (Laboratory of Environmental Comfort and Energy Efficiency) and worked at GCP Arquitetura e Urbanismo. Later, I cofounded [estúdio greta](https://www.estudiogreta.com.br/) and now I am exploring digital fabrication tools as a new way of creation and production. As a student from the Master in Design for Emergent Futures at IAAC, I am increasingly interested in how to create local connection through making in the context of the redistributing manufacturing world.
<br>
<br>
### What is my initial idea for Fab Academy final project?
<br>
<br>
"Efficiency and self-sufficiency will become the new standard as we move from the stage of consumers towards harvesters." Statement from Power Play exhibition (2016), from Dutch Invertuals.
<br>
<br>
I think that the current model of living needs deep changes and we are already late in changing our behaviors. Becoming self-sufficient more and more might be a possible alternative for our future. Therefore, for the Fab Academy final project I am interested in exploring the universe of solar power, starting by a small scale, doing a solar lamp. I also see potential for a redistributed civic lighting network in public spaces of informal neighborhoods, like the favelas that we have in Brasil. Such spaces are narrow and dark, creating insecurity in the people walking those alleys. If people install solar lamps in the entrance of their houses, they could help for a more secure path for the pedestrians.
<br>
<br>
My inspiration from the project comes from the Little Sun Foundation, which has solar lamps developed by artist Olafur Eliasson. The lamps help people in Africa to access lighting off the grid.
<br>
<br>
{% figure caption: "*two models of solar lamp by Little Sun*" %}
![]({{site.baseurl}}/w01_01.jpg)
{% endfigure %}
<br>
<br>
![]({{site.baseurl}}/w01_02.jpg)
<br>
<br>
![]({{site.baseurl}}/w01_03.jpg)
