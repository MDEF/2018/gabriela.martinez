---
title: 08 • Computer-controlled Machining
period: 06-13 March 2019
date: 2019-03-13 12:00:00
term: 2
published: true
---


<br>
<br>
Assignment: make (design+mill+assemble) something big - extra challenge: no glue, no screws or nails
<br>
<br>
To start this week, we had [lectures](http://fabacademy.org/2019/labs/barcelona/local/wa/week7/) with local tutors to learn the many details involved in preparing a file to be cut on a CNC machine. Since I have been working with CNC for a while now, in my studio work - [estúdio greta](https://www.estudiogreta.com.br/) - I am familiar with most of the parameters presented this week. Anyhow, there are details that are particular from each machine and software, and for me, working with RhinoCAM to prepare the g-code is something new and interesting.
<br>
<br>
This week included designing a project, preparing the file for the machine, milling it and assembling the project. I decided to do a small table for my room here in Barcelona, so I can work from there, since it is a small room. The idea is that the table stands only by its joints and so, I can have two different sizes of legs, making it possible to be a table or a support for my computer to be placed over the bed. I wanted to keep exploring new softwares for me, as Fusion 360 and Rhino, but I was short on time this week and ended up going for the softwares that I normally use to design - SketchUp and Autocad. I first did models in Sketchup and refined the joints on Autocad, creating the final project. The constraints for this project were due to the material and tools: we had a plywood board with 15mm of thickness and some end mill diameters to consider (up to 6mm). This information is key when it comes to design the joints of pieces. Normally it is considered an extra 0.5mm margin for the joints’ width. Also, it is needed to consider the diameter of the cutting tool - the end mill - so the joints will fit well. In the design, we incorporate what is called bones, small reentrances due to the tool’s diameter. I chose to cut with an 6mm end mill, so my bones had a 6.1mm diameter - a little margin is considered so the machine understands it is possible to cut.
<br>
<br>
{% figure caption: "*Final design*" %}
![]({{site.baseurl}}/fa_w08_01.jpg)
{% endfigure %}
<br>
<br>
To confirm that the design was correct, I did a model of it, cutted in the laser. All the joints were right sized and positioned.
<br>
<br>
{% figure caption: "*Laser cutted model*" %}
![]({{site.baseurl}}/fa_w08_02.jpg)
{% endfigure %}
<br>
<br>
With the 2D drawings of the pieces on Autocad, I saved a .dxf file that I opened on Rhino to generate the g-code on RhinoCAM. To prepare the file to mill, it is needed to generate the g-codes for the cutting paths. I shared the plywood board with Veronica, and we placed our projects together and also added some minor projects as a way to make the most usage of our plywood board. The board was sized 2500x1250mm with 15mm of thickness. We left a 20mm margin on all sides to place screws and some more inside the board accordingly to free space left in between pieces. The space in between pieces left was of 25mm. During the cut, it was noticeable that the border margin was too tight for screws and the end mill passing. For next time, a larger margin should be considered.
<br>
<br>
After doing the nesting of the board, we started to prepare the files for cutting. First, we prepared the file to make the marks for placing the screws - the way we would fix the board on the machine. For that file, the parameters on the RhinoCAM plugin used were:
<br>
<br>
Machining operation:
<br>
2 axis > engraving (to mark the material for screws) > select drive containment (select the points)
<br>
Select tool: flat endmill 6mm
<br>
Feeds & speeds:
<br>
Spindle speed:18000
<br>
Cut: feed rate for plywood: 5000mm/min
<br>
Plunge 3000mm/min (all other values go equal to plunge)
<br>
Transfer: use rapid
<br>
Transfer speed: 10.000
<br>
Clearance plan: stock max Z - dist: 10
<br>
Cut parameters:
<br>
Total cut depth: 1
<br>
Rough depth/ cut :  1
<br>
Sorfing: minimum distance
<br>
<br>
{% figure caption: "*Engraving path*" %}
![]({{site.baseurl}}/fa_w08_03.jpg)
{% endfigure %}
<br>
<br>
Next toolpath was the one to make pockets on the plywood. The parameters defined were:
<br>
Feeds & speeds - same value
<br>
Clearance plane - same
<br>
Cut parameters:
<br>
Cut direction: climb
<br>
Start point : inside / outside
<br>
Stepover: 40%
<br>
Cutt pattern: offset spiral
<br>
Cut levels:
<br>
Total cut depth: 10
<br>
Rough depth: 3 / half of the cut
<br>
Entry exit - ramps
<br>
Sorfing: not
<br>
<br>
{% figure caption: "*Pocketing path*" %}
![]({{site.baseurl}}/fa_w08_04.jpg)
{% endfigure %}
<br>
<br>
Next toolpath was to make holes, for Veronica’s stool design:
<br>
Feeds - use rapid
<br>
Cut levels:
<br>
Total cut depth (16)
<br>
Rough depth - same total cut
<br>
Rough cut - half of the end mill (3.2)
<br>
entry/exit: along path 3D entry / Exit motion:none
<br>
Advanced cut parameters:
<br>
bridge/tabs - create bridges - numbers: 4 / height: 3 length: 4
<br>
Sorting: minimum distance
<br>
<br>
{% figure caption: "*Holes path*" %}
![]({{site.baseurl}}/fa_w08_05.jpg)
{% endfigure %}
<br>
<br>
Then we proceed to make toolpath for the profiling, for incuts and outcuts. They had the same parameters, only changed in the orientation of the cut - inside the line for incuts, outside the line for outcuts.
<br>
Feeds - use rapid
<br>
Cut levels:
<br>
Total cut depth (16)
<br>
Rough depth - same total cut
<br>
Rough cut - half of the end mill (3.2)
<br>
Entry/exit: along path 3D entry / Exit motion:none
<br>
Advanced cut parameters:
<br>
Bridge/tabs - create bridges - numbers: 4 / height: 3 length: 4
<br>
Sorting: minimum distance
<br>
<br>
{% figure caption: "*Incuts path*" %}
![]({{site.baseurl}}/fa_w08_06.jpg)
{% endfigure %}
{% figure caption: "*Outcuts path*" %}
![]({{site.baseurl}}/fa_w08_07.jpg)
{% endfigure %}
<br>
<br>
WIth the toolpaths ready, we separated them in two g-code files: one for the engraving (for placing the screws) and other for all the procedures of milling.
<br>
<br>
Next step was to cut: we headed to the machine. First, we changed the end mill for the one we wanted to cut: a 6mm downcut flat end mill. In this process it is also needed to change the collet according to the diameter of the end mill, so it will hold it properly in place in the machine. Then we placed the board in the machine and set X and Y by moving the axis manually and the Z with the automated button to define it. With the safety tips in mind (protective glasses, hair tied and safe distance of the machine), we run the first file to mark the points for the screws. Later, we fixed the screws in place and started the file for pocketing, making holes and profiling. The cut started really well but we learned some things in the process: the speed was to slow, so we remade the file and started again with a better speed. Also, the bridges we made were a bit too small and some pieces started to come off. We needed to stop the machine some times to take these pieces that were coming out.
<br>
<br>
{% figure caption: "*Cutting process*" %}
![]({{site.baseurl}}/fa_w08_08.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Cutting process*" %}
![]({{site.baseurl}}/fa_w08_09.jpg)
{% endfigure %}
<br>
<br>
Therefore, from the process of cutting, it was possible to envision improvements for next cuts:
<br>
• Bigger outer margin on the board
<br>
• More screws to hold the board if it is slightly bended
<br>
• Bigger bridges
<br>
<br>
After finishing the cut, I proceeded to sand the pieces in order to take out some wood splinters. The act of sanding also helped to make joints fit better, since they felt a bit too tight in the beginning. After this, I assembled the table only by its joints, without the need of glue or screws. By using only the joints to assemble it is possible to use the table as explained before, with two different sizes of legs, one to stand over the floor and another over the bed.
<br>
<br>
{% figure caption: "*Assembled at Fab Lab*" %}
![]({{site.baseurl}}/fa_w08_10.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*In use with long legs*" %}
![]({{site.baseurl}}/fa_w08_11.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*In use with short legs*" %}
![]({{site.baseurl}}/fa_w08_12.jpg)
{% endfigure %}
<br>
<br>
**Download the files**
<br>
<br>
[w08_computer-controlled_machining_design_model - skp file]({{site.baseurl}}/w08_computer-controlled_machining_design_model.skp)
<br>
[w08_computer-controlled_machining_design - dxf file]({{site.baseurl}}/w08_computer-controlled_machining_design.dxf)
<br>
[w08_computer-controlled_machining_board - 3dm file]({{site.baseurl}}/w08_computer-controlled_machining_board.3dm)
