---
title: 02 • Project Management
period: 23-27 January 2019
date: 2019-01-23 12:00:00
term: 2
published: true
---


<br>
<br>
Assignment: learn about Git and build your personal website
<br>
<br>
I am part of the Master in Design for Emergent Futures, from IAAC and Elisava in Barcelona, Spain. Our master course started in October 2018 and since then, we have created our personal websites for the master’s documentation and we will be using them for the Fab Academy. In this documentation, I will present the guides for the Git tutorial and the changes I have made in my website to include a section dedicated to the Fab Academy.
<br>
<br>
First, we had a Git tutorial guidance by Fab Lab Barcelona tutors. They showed us how to set our git-based websites. We installed Git and as a platform of choice, we are operating with GitLab.
<br>
<br>
How to set a website based on GitLab:
<br>
<br>
First, create an account on GitLab. You will have now an online repository to work on your website. To be able to work on your computer, locally, and make changes from there, you will need to clone the repository in your computer and work through an editor. To make these processes in your computer, you will need an interface to work, like a command window. To used Git in Windows, I am using Git Bash. To clone your repository in your computer, you first need to authorize this connection. You will generate your SSH Key, a personal code from your computer.
<br>
In Git Bash, generate a SSH Key, with the code:
<br>
ssh-keygen –t rsa –C “$my_email”
<br>
Later, with:
<br>
cat ~/.ssh/id_rsa.pub
<br>
You will get the full SSH Key of your personal computer. With this, add the SSH Key on your git-page, in the *user settings* page, add this personal key generated in your computer. You now authorized this connection between the online repository and the local repository in your computer.
<br>
Now, you will be able to clone your GitLab repository on your computer. First, set the folder you will like to make the copy in Git Bash by typing:
<br>
cd “path to the folder”
<br>
Later, type:
<br>
git clone
<br>
With the repository also in your computer, you can work with an editor to make the changes on your website. My editor of choice was Atom. With Atom, we can make the same processes that we could do online on GitLab. Atom also has the tools to make the actual changes on the website, just as also GitBash makes it possible. After making changes in the files of your website, to publish it online, you need to follow the steps:
<br>
•Stage changes
<br>
•Commit changes (Add a description! This will help you remember the changes you made when you look at the branch of changes of your version control website)
<br>
•Push changes
<br>
Our websites are already running with Jekyll, which is a static site generator. You need to install Jekyll and Gemfiles from Ruby to have a website with this structure.
<br>
In my current website, I made changes to include a section dedicated to Fab Academy. I created a subpage based on the same template I had for including the master’s reflections. I created a new html file based on this template:
{% figure caption: "*fab-academy.html file*" %}
![]({{site.baseurl}}/w02_01.jpg)
{% endfigure %}
I made the changes needed in this and other files to refer to the content I will add related to Fab Academy. Besides the new html, I created a markdown file for Fab Academy, where all content will be listed, and made the necessary changes on the header.html file to have a link for Fab Academy in the homepage. Also needed to make deeper changes on the config.yml file, which is the one that “runs” the website.
<br>
{% figure caption: "*fab-academy.md file*" %}
![]({{site.baseurl}}/w02_02.jpg)
{% endfigure %}

{% figure caption: "*header.html file*" %}
![]({{site.baseurl}}/w02_03.jpg)
{% endfigure %}

{% figure caption: "*changes in the config.yml file*" %}
![]({{site.baseurl}}/w02_04.jpg)
{% endfigure %}

{% figure caption: "*Homepage with new section for Fab Academy*" %}
![]({{site.baseurl}}/w02_05.jpg)
{% endfigure %}

<br>
Glossary:
<br>
<br>
Git: distributed version control system for tracking changes in source code during software development.
<br>
<br>
Version Control: a type of management of changes in documents, computer programs, large websites and other collection of information.
<br>
<br>
GitLab: web-based Git-repository manager providing wiki, issue-tracking and CI/CD pipeline features, using an open-source license.
<br>
<br>
Jekyll: a static site generator written in Ruby programming language.
<br>
<br>
Static site generator: collection of pages contained in basic HTML files.
<br>
<br>
HTML: Hypertext Markup Language is the standard markup language for creating web pages and web applications
<br>
<br>
Markup Language: a computer language that uses tags to define elements within a document. It is human-readable, meaning markup files contain standard words, rather than typical programming syntax. While several markup languages exist, the two most popular are HTML and XML.
