---
title: 04 • Material Driven Design
period: 14-15 February 2019
date: 2019-02-15 12:00:00
term: 2
published: true
---


<br>
<br>
The next experimentations I focused more on the avocado seed, although I tried one more experiment with the peels. After letting them dry naturally, I made it into a flour with a pestle and mortar, then cooked it with water to see if it would bind together (P-004). It did not bind, so I decided to focus my exploration into the seed.
<br>
<br>
To avoid the mold from appearing in my samples, I started to add vinegar to the recipes. I did different explorations with only the material but also started to test new recipes with added binders and/or plastifiers.
<br>
<br>
I kept the process of the previous experiments of transforming the seed into flour by shredding it and blending it. I did one sample of the flour with water (S-015) and let it dry to see if would bind. It did not. In another one (S-014), I added pine resin and put in the oven. The amount of pine resin was too little to bind all the flour. I cooked the flour with water, making that pudding element again and tried to pass by a syringe (S-008). The syringe available in the lab had a small nozzle for the density of the material, so it had a lot of pressure inside and I was not able to press much material through it. I also use this mixture of cooked avocado seed with water to put in different molds and meshes and see how it would react. Most of the times, after drying in the mold or mesh, the sample would develop some cracks. I boiled some avocados to analyse this process and see if I could extract some starch from it. The results were not conclusive.
<br>
<br>
Moreover, I explored adding binders and/or plastifiers to this mixture of avocado seed flour cooked with water. First, I explored different proportions of binder and plastifier from a recipe of bioplastics I learned from Barbara Sanchez, an intern in the Fab Lab Barcelona. This recipe consisted in adding glycerol and agar agar. Also, I tried other alternatives, with edible binder like xanthan gum. With these tests, I tried different supports so they could dry, going 3D with molds and meshes. All of them resulted in nice new materials, the only thing related to the bioplastics is that as the water would evaporate, the pieces would shrink in size. The ones with xanthan gum, this was not much a problem.
<br>
<br>
My conclusions about all the experiments will be presented in the post dedicated for the final presentation. Below, are indicated all the recipes for these new experiments done during this week.
<br>
<br>

![]({{site.baseurl}}/mdd_w04_p004.jpg)

![]({{site.baseurl}}/mdd_w04_s008.jpg)

![]({{site.baseurl}}/mdd_w04_s009.jpg)

![]({{site.baseurl}}/mdd_w04_s010.jpg)

![]({{site.baseurl}}/mdd_w04_s011.jpg)

![]({{site.baseurl}}/mdd_w04_s012.jpg)

![]({{site.baseurl}}/mdd_w04_s013.jpg)

![]({{site.baseurl}}/mdd_w04_s014.jpg)

![]({{site.baseurl}}/mdd_w04_s015.jpg)

![]({{site.baseurl}}/mdd_w04_s016.jpg)

![]({{site.baseurl}}/mdd_w04_s017.jpg)

![]({{site.baseurl}}/mdd_w04_s018.jpg)

![]({{site.baseurl}}/mdd_w04_s019.jpg)

![]({{site.baseurl}}/mdd_w04_s020.jpg)

![]({{site.baseurl}}/mdd_w04_s021.jpg)

![]({{site.baseurl}}/mdd_w04_s022.jpg)

![]({{site.baseurl}}/mdd_w04_s023.jpg)

![]({{site.baseurl}}/mdd_w04_s024.jpg)
