---
title: 01 • Material Driven Design
period: 24-25 January 2019
date: 2019-01-25 12:00:00
term: 2
published: true
---


<br>
<br>
Material Driven Design started on 24/01/2019 with a lecture from Mette Bak-Andersen and Thomas Duggan, our tutors for this course. Mette Bak-Andersen is the founder of [Material Design Lab](http://materialdesignlab.dk/) at KEA, Copenhagen School of Design & Technology and a PhD Fellow at The Royal Danish Academy of Fine Arts, School of Design, KADK. Thomas Duggan leads his own practice that crosses design, nature and technology. A wide range of projects he created can be seen in [his website](http://thomasdugganstudio.com/).  The objective of this course will be to test a design process based on a material dialogue in the context of digital fabrication.
<br>
<br>
In this first class, tutors started with reflections about sustainability and circular economy. We discussed in groups what do we think that sustainability means and the role of the designer in the creation process. Bak-Andersen then gave us a lecture about the history of materials in the design education and the material dialogue in craft. Later, Duggan presented us his work, under the title of Technological Craft.
<br>
<br>
The next day we went to Valldaura Labs for an exercise of material dialogue. Jonathan Minchin guided us in the forest, explaining the vegetation that exists in the area, a pine tree forest and a oak tree forest. We walked in both forests, learning about plants and the landscape transformations that humans made there during different times.
<br>
<br>
We had different types of wood to start our exercise of material dialogue  - to carve a spoon out of a wood piece. I chose the laurel wood. We spent around 6 hours working in our spoons but impression we had was that the time passed really fast and that it was less than this. The activity was really pleasant and to acknowledge the difficulties a material can present to us was really value for the next activities of the course. To work with our hands and tools, sometimes with limitations, over a material that can surprise us with different features was a great process. We could understand the material and its features, that could be the guide for a decision in the process of altering it. I was surprised by the result of my spoon, at first I was worried by hurting myself with the tools, but this has not happened. It was really nice to acknowledge the time it takes to work over a material with our hands, without any facilitation from modern technologies. Also, to conclude the day by sharing our impressions and feelings from doing the exercise was an enriching experience of listening from each other’s subjective view.
<br>
<br>
![]({{site.baseurl}}/mdd_w01_01.jpg)
