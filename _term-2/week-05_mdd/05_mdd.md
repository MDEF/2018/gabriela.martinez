---
title: 05 • Material Driven Design
period: 21 February 2019
date: 2019-02-21 12:00:00
term: 2
published: true
---


<br>
<br>
For the final presentation, I showed the raw material and the samples with the information of the recipes. I could find more in detail the composition of the Hass avocado seed, which is indicated below.
<br>
<br>
![]({{site.baseurl}}/mdd_w05_01.jpg)
<br>
<br>
![]({{site.baseurl}}/mdd_w05_02.jpg)
<br>
<br>
Most of my initial samples got mold, a characteristic I was not expecting so soon (most got it from one day to another). These ones I threw away. The presented samples were the ones that I started to add vinegar to the process. Since my other posts about the process already have the specific recipes for each sample, here I will focus on commenting my impressions for the whole exercise of material dialogue.
<br>
<br>
The samples that I believe gave me the best result were the ones based on avocado seed flour, water and xanthan gum. The outcome was a resistant shape, moldable while still wet and edible. The fact that can be consumed gives the possibility of developing a line of edible products to replace disposable utensils, such as cutlery and plates.  
<br>
<br>
![]({{site.baseurl}}/mdd_w05_03.jpg)
<br>
<br>
I had many expectations with the bioplastic recipe, hoping it would give me nice results in 3D shape. The results were interesting but not applicable for products, I believe. By the fact that in the process of drying, the samples shrinked, changed shapes and got cracks, it is difficult to make a useful product out of this process.
<br>
<br>
![]({{site.baseurl}}/mdd_w05_04.jpg)
<br>
<br>
![]({{site.baseurl}}/mdd_w05_05.jpg)
<br>
<br>
A failed process was the one intended to create shapes just out of the material, after cooking avocado seed flour with water. I tried with different proportions, shapes and amount of material to dry, but all of them presented cracks during the process of drying.
<br>
<br>
![]({{site.baseurl}}/mdd_w05_06.jpg)
<br>
<br>
I concluded the exercise considering that the best sample was the one mixed with xanthan gum as mentioned above. From the overall exercise, I believe that important lessons were learned. The process of manipulating a material in the context of a lab needs rigorous documentation and lots of trials and errors to find possible paths.
<br>
<br>
Also, the physical contact with the material gives us another dimension of comprehension. We start to observe what we have in hands to then let it lead the process of design, a methodology that I was never in touch before. This made me reflect about all the years that I worked in an architecture office and I was mostly drawing projects and not being in contact with the materials that would some day materialize that project. The lack of material dialogue/contact at an initial stage of designing in the architecture, design or creative studios is something that needs to be addressed, in order to rebalance our relationship with design and the environment. An increased contact with the materials that shape our world can bring awareness of the importance of the conscious choice, making us reflect about the lifespan of the materials we use and the impact they create. Moreover, materials can gives us information about how to conduct the design process, flourishing solutions that were never thought before. I really appreciate the way of thinking discovered during this course and I will certainly include this knowledge in the realm of my practice.
<br>
<br>
![]({{site.baseurl}}/mdd_w05_07.jpg)
