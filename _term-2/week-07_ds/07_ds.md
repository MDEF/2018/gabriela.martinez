---
title: 07 • Design Studio
period: 05 March 2019
date: 2019-03-05 12:00:00
term: 2
published: true
---


<br>
<br>
Midterm - feedback from Tomas Diez, Mara Balestrini and Ingi Freyr
<br>
<br>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQSDXpXWcPVIscifiwoQ0c4H2jT38CPakWHbdoEwOUpgPWplBxftJwnal-AfobutGnwvt7FvlHe_xGs/embed?start=false&loop=false&delayms=3000" frameborder="0" width="680" height="424" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<br>
<br>
