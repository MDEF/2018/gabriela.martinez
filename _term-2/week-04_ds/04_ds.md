---
title: 04 • Design Studio
period: 12 February 2019
date: 2019-02-12 12:00:00
term: 2
published: true
---


<br>
<br>
 Review with Mariana
<br>
<br>
-- How can people connect to the place that they are and its identity? ¿How can people connect with this identity or feel part of it?
<br>
<br>
-- Is it important to feel connected to a place?
<br>
<br>
  -- Because the impact of existing in a place
<br>
<br>
  -- Because the changes that will come with climate change and the relationship with place
<br>
<br>
 -- Does the concept of ritual play a role in this?
<br>
<br>
 -- the homogenization vs. the singular f.e see in the case of cultural celebration
<br>
<br>
 -- Concepts in anthropology can help inform this question, but you didn't know how this point of view would apply to your practice
<br>
<br>
-- how can we explore this relationship of people, place and materials in the context of fab labs as spaces of making?
<br>
<br>
--  Each place has its own craft, and materials that you can source
<br>
<br>
-- Can I understand better the crafts of the Barcelona region, in relationship to the material and the people?
<br>
<br>
-- Visit museums, read bibliography, history of crafts in Cataluña?
<br>
<br>
-- How does globalization (when the world is a factory) affect this relationship between place/material/crafts/people?
<br>
<br>
-- but what is local nowadays? globalization requires new thinking of what "local materials mean"
<br>
<br>
 {% figure caption: "*caption*" %}
 ![]({{site.baseurl}}/image.jpg)
 {% endfigure %}
