---
title: 01 • Atlas of Weak Signals
period: 08 March - 11 April 2019
date: 2019-04-11 12:00:00
term: 2
published: true
---


<br>
<br>
Atlas of Weak Signals (08.03.2019 - 11.04.2019) was a course divided in two parts: Definition and Development. On the Definition track, we explored the most rising weak signals of our times with tutoring of Mariana Quintero and José Luis de Vicente. On the Development track, tutored by Ramón Sangüesa and Lucas Lorenzo Peña, we learned how to create our visual data repository of these possible trends with data scraping.
<br>
<br>
The lectures of the Definition track, presented to us the most urgent problems related to the environment, politics, economy, technology and society. In each class, we were presented to 5 weak signals that would compound our repository, with 25 in total. Below, these weak signals are listed:
<br>
<br>
**Life in the times of Surveillance Capitalism**
<br>
• Attention protection
<br>
• Dismantling filter bubbles
<br>
• Circular data economy
<br>
• Truth Wars
<br>
• Redesigning social
<br>
<br>
**Designing for the Anthropocene**
<br>
• Climate conscience
<br>
• Inter-species collaboration
<br>
• Long-termism
<br>
• Carbon neutral lifestyles
<br>
• Fighting Anthropocene conflicts
<br>
<br>
**Life after AI - the end of work**
<br>
• Technology for equality
<br>
• Fighting AI bias
<br>
• Imagining new jobs
<br>
• Making Universal Basic Income work
<br>
• Human-machine creative collaborations
<br>
<br>
**After the nation-state**
<br>
• Making world governance
<br>
• Rural futures
<br>
• Pick your own passport
<br>
• Refugee tech
<br>
• Welfare state 2.0
<br>
<br>
**Kill the heteropatriarchy**
<br>
• Non-heteropatriarchal innovation
<br>
• Imagining futures that are not western-centric
<br>
• Reconfigure your body
<br>
• Gender fluidity
<br>
• Disrupt ageism
<br>
<br>
In each of the 5 general groups, we had 5 smaller topics indicating the weak signals of our times. During the Definition track, we were divided in groups and developed scenarios for the potential occurrence of one of these conditions in the future. I was part of the following groups: Attention protection, Inter-species collaboration, Imagining new jobs and Refugee tech. The last big topic, about body and identity, was not discussed in group sessions. It was a very interesting and sometimes challenging exercise to come up with the futures’ scenarios. Science fiction narratives were tools of inspiration, so we could imagine the artifacts, environments and relationships happening in that hypothetical scenario. It was a valuable process to let imagination run free and also to spot possible contradictions and difficulties if such scenarios would happen.
<br>
<br>
In the Development track, with these weak signals in hand, we started to create a repository of articles and news that point out the occurrence of such events in near or far futures. Not only we were collecting them, but also used tools to extract the core of the topics, through keywords. It was really valuable to get to know the tools to do this, as for me they were all new. I learned that there is an environment of collective coding provided by Google, the [Colaboratory](https://colab.research.google.com/notebooks/welcome.ipynb#recent=true), learned the principles of data scraping in Python, with libraries that make it possible like [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/), and also got to know [Algorithmia](https://algorithmia.com/), a service of community contributed machine learning models. Also, the group work for making it possible, the more experienced ones sharing knowledge, and the amazing effort of Saira into building the visual outcome of this repository was really uplifting.
<br>
<br>
{% figure caption: "*Atlas of weak signals map by Saira Raza. For interactive visualization, click [here.](https://kumu.io/sraza/materials6#aowsscraped)*" %}
![]({{site.baseurl}}/aws_01.jpg)
{% endfigure %}
<br>
<br>
During this course, I was reflecting much upon the question of “Who talks about the future?” and to whom it is communicated. There are plenty of companies of Trend Forecasting but they are mostly concerned to talk to the fashion industry and other design fields. “Who is talking of futures to people/society? Who works along governments to envision the problematic situations concerning climate, economical and political(representative) issues of the future?” Obviously we have scientists working on such predictions, but this is not a language that communicates directly to society. Who is doing the bridge of all the data about the current state of the world, also its future, and society? I have not found the answer to this yet, but became familiar to some design studios or agencies that are addressing some of these topics in a more understandable communication.
<br>
<br>
Personally, this course was really interesting to me since I like to research on possible trends and novelty. It was interesting to see news and articles confirming the path of how things are going, showing many troubled situations that we are facing, but mainly it was interesting to see possible scenarios that are more positive, distancing from dystopian constructs of realities. Yet, for positive futures, there is the need of many people engaging in changes. And there is where it lies my biggest interest - in changes. For a less environment-impactful reality, a more plural and diverse expression of narratives from people, new ways of making and understanding work.
<br>
<br>
Therefore, for my research, I see as main weak signals the ones that relate to place/environment (climate conscience, carbon neutral lifestyles, long-termism) and to people (redesigning social, imagining new jobs, imagining futures that are not western-centric). I believe that exploring them even further will bring new concepts and insights for my project, related to places, identity, belonging and local narratives. These are topics that interest me deeply beyond the project itself, since I believe that we need to empower people with their own voices and spaces of creation in opposition to the authoritarian and destructive forces of the global capitalism.
