---
title: 01 • Reflections Term 1
period: 01 January 2019
date: 2019-01-01 12:00:00
term: 2
published: true
---


<br>
<br>
![]({{site.baseurl}}/p_t01_01.jpg)
<br>
<br>
As a sum of the first term, I can say that I have got in touch with many different topics that were interesting. Some of them were totally new to me while others added new information to topics I was already aware. Some of the weeks were also relevant to bring reflections over my final project, and I highlight weeks like [Exploring Hybrid Profiles in Design](https://mdef.gitlab.io/gabriela.martinez/term-1/exploring-hybrid-profiles-in-design/), [Navigating the Uncertainty](https://mdef.gitlab.io/gabriela.martinez/term-1/navigating-uncertainity/), [The way Things Work](https://mdef.gitlab.io/gabriela.martinez/term-1/the-way-things-work/) and [Living with Ideas](https://mdef.gitlab.io/gabriela.martinez/term-1/living-with-ideas/) as the most relevant ones for my area of interest.
<br>
<br>
Concluding the term with [Design Dialogues](https://mdef.gitlab.io/gabriela.martinez/term-1/design-dialogues/) was a good way to receive feedback and bring new ideas to move forward with the project. At that moment, I defined as a topic of research: **local connection through making in the context of the redistributed manufacturing world**.
<br>
<br>
![]({{site.baseurl}}/p_t01_02.jpg)
<br>
<br>
From the feedback I received, I could confirm that this topic is interesting and has relevance to our current times, but I still could not define where to move forward. The feedbacks indicated some possible paths: develop a toolkit to orient local communities in the process of creating this new relationship of connection to their place, to look for the material disconnection we currently have and the implications of this to our living future, to envision how consumption can change with a new model of relationship, to understand the challenge of re-globalizing the world and to look for craft in the making process - the cultural and historical trajectory it contains.
<br>
<br>
To look forward on the project, I had interest on the idea of developing the toolkit and exploring the material disconnection we currently have. With the project developed on Barcelona and the research trip to Copenhagen I could receive insights in both areas. This will be further commented on the post dedicated to this period.
