---
title: 10 • Bibliography
period: 14 June 2019
date: 2019-06-14 06:00:00
term: 3
published: true
---


<br>
<br>
Adichie, CN. (2018). El Peligro de la História Única. Barcelona, Spain: Literatura Random House.
<br>
Dias, R. & Smith, A. (2018). Making in Brazil: can we make it work for social inclusion? In Journal of Peer Production, 12 (1). pp. 43-59. ISSN 2213-5316
<br>
Eglash, R. (2018). Decolonizing Digital Fabrication: Case Studies in Generative Justice. In T., Diez (Ed.), Fab Cities - The Mass Distribution of (Almost) Everything. pp. 46-61. Barcelona,Spain: Distributed Design. https://issuu.com/iaac/docs/book-fabcity__2oct_
<br>
Lévy, P. & Yamada, S. (2017). 3D-modeling and 3D-printing explorations on Japanese tea ceremony utensils. In Proceedings of the 11th International Conference on Tangible, Embedded and Embodied Interactions, TEI17 ([on CD]). Yokohama, Japan: ACM Press. https://doi.org/10.1145/3024969.3024990
<br>
Lévy, P. (2015). Exploring the challenge of designing rituals. In V., Popovic, A., Blackler, & B., Kraal (Eds.), the Proceedings of 6th International Congress of International Association of Societies of Design Research, IASDR 2015 ([on CD]). Brisbane, Australia: Queensland University of Technology.
<br>
Lindtner, S., Shaowen, B. & Jeffrey, B. (2018). Design and Intervention in the Age of "No Alternative". In Proceedings of the ACM on Human-Computer Interaction, Vol. 2, CSCW, Article 109 (November 2018). ACM, New York, NY. 21 pages. https://doi.org/10.1145/3274378
<br>
Lippard, L. (1997). The Lure of the Local. New York, USA: The New Press.
<br>
Nachtigall, T., Tomico, O. & Wakkary, R. (2017). ONEDAY Shoes: A Maker Toolkit to Understand the Role of Co-Manufacturing in Personalization. In Proceedings of the 13th International Conference on Tangible, Embedded and Embodied Interaction. ACM, New York, NY. Pages 105-115. https://doi.org/10.1145/3294109.3295637
<br>
Smith, A. (2014). Technology networks for socially useful production. In Journal of Peer Production (5). ISSN 2213-5316
<br>
Sennett, R. (2008). The Craftsman. New Haven, USA: Yale University Press.
<br>
Sterling, B. (2005). Shaping Things. Cambridge, USA: MIT Press.
<br>
Trotto, A. (2011). Rights Through Making - Skills for Pervasive Ethics. Eindhoven, Netherlands: Eindhoven University of Technology.
<br>
<br>
Websites
<br>
<br>
Pottery from Serra da Capivara. Retrieved from http://www.artesol.org.br/rede/membro/grupo_ceramica_artesanal_serra_da_capivara
<br>
Serra da Capivara UNESCO heritage. Retrieved from https://whc.unesco.org/en/list/606
<br>
Serra da Capivara National Park. Retrieved from https://en.wikipedia.org/wiki/Serra_da_Capivara_National_Park
<br>
Pottery from Serra da Capivara. Retrieved from http://ceramicacapivara.com/catalogo.asp
<br>
Japanese Ceramic Towns. Retrieved from https://japanobjects.com/features/japanese-ceramic-towns
<br>
Pottery from Mashiko. Retrieved from http://www.explorejapaneseceramics.com/category/towns/mashiko
<br>
Pottery from Mashiko. Retrieved from http://www.mashiko-kankou.org/english/mta1/mashikoyaki/sankoukan/sankoukan.htm
<br>
Tableware pottery from Mashiko. Retrieved from http://en.diningpottery.jp/jdp-8662.html
<br>
Pottery from Morocco. Retrieved from https://www.journeybeyondtravel.com/blog/morocco-pottery-art.html
<br>
Pottery from Safi. Retrieved from https://marocmama.com/safi-the-pottery-capital-of-morocco/
<br>
Tableware pottery from Safi. Retrieved from http://www.moroccandecor.co.uk/Handmade-Moroccan-Ceramic-Safi-Pottery-Plate
<br>
Fab Labs map. Retrieved from https://www.fablabs.io/labs/map
<br>
Fab Labs in Tokyo. Retrieved from http://www.makery.info/en/2014/10/07/a-tokyo-les-fablabs-sont-plus-fab-que-lab/
<br>
Niede Guidon about Serra da Capivara National Park. Retrieved from http://artenarede.com.br/blog/index.php/niede-guidon-a-saga-da-serra-da-capivara/
<br>
El Mundo article about Serra da Capivara. Retrieved from https://www.elmundo.es/ciencia/2015/07/27/55b4ff0022601d4e538b4570.html
<br>
Interview with Moulay Ahmed Serghini. Retrieved from https://www.youtube.com/watch?v=CrXKGEuqgVE
