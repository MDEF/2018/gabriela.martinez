---
title: 06 • Futures' Projection
period: 14 June 2019
date: 2019-06-14 02:00:00
term: 3
published: true
---


<br>
<br>
**Project your intervention into the future**
<br>
<br>
The project intends to redefine production as a lively practice of connection and circularity inside the urban fabric through an exploration of the hybrid space in between the Commons knowledge of craftsmanship and the new emerging technologies for making. A projection of it into the future goes by discussing the wider ideas contained in this project - concepts, technologies and societal changes that will impact these futures.
<br>
<br>
*Concepts*
<br>
<br>
• Circular Economy
<br>
<br>
As defined by the Ellen MacArthur Foundation, circular economy is a framework for an economy that is restorative and regenerative by design. It looks beyond the take-make-waste practice of the current extractive industrial model, looking to redefine growth, benefiting positively the society. It is a systemic shift to be applied in all scales - from small to large companies, from individuals to organizations, locally and globally. It should build long-term resilience and can be based in two models, a biological cycle and/or a technical cycle. The biological one functions as a living system, considering regeneration in the process and provision of renewable resources. The technical model proposes strategies of reuse or restoration, by maintaining and reusing, repairing, remanufacturing and in a last scenario, recycling. The Foundation also highlights the importance of digital technology to support this transition, by “radically increasing virtualisation, dematerialisation, transparency, and feedback-driven intelligence”.
<br>
<br>
• Micro Manufacturing
<br>
<br>
Rapid prototyping machines were tools for the industry to do a first iteration with products. They started to become accessible to a wider public due to patent expirations and collaborative/open source projects to design and build less complex machines. From this scenario, Fab Labs - fabrication laboratories - emerged and spread throughout the world. These spaces are small scale workshops providing digital fabrication tools. While they are a potent platform to empower citizens and enable production with a high level of personalization, they lack a strategy of local production, of being economically viable, and allowing local expressions for material usage and cultural creativity. Since they follow a standardized requirement of machines, these tools also limit the material usage, making people recurrently use standardized materials that are produced in faraway lands. It is imperative that these spaces start to look for the uncovered potential of materials available locally. Besides Fab Labs, people accessing these tools are setting makerspaces independently. Most of them function in a coworking structure, to enable its sustainability as a business. Some of them also offer the services of manufacturing to keep on existing. These micro manufacturing spaces are in the in between of industry and crafts, allowing production in sort of a scale but yet maintaining attention to details and possibility of customization.
<br>
<br>
{% figure caption: "*Micro manufacturing - ©Florent Gardin for Atelier Luma*" %}
![]({{site.baseurl}}/p_t03_micromanufacturing.jpg)
{% endfigure %}
<br>
<br>
• Fab City
<br>
<br>
A concept developed from the high potential of transformation that Fab Labs have, Fab City is scaling this approach to cities and systems. The intention is to help citizens and cities to become locally productive and globally connected. By creating a network of initially 28 cities, the Fab City Foundation is establishing the parameters and procedures for cities to become self-sufficient by 2054, a year in which 70% of global population will be living in cities. The project embraces the strategies of circular economy, digital social innovation and has a manifesto with its values and visions. The manifesto encompasses a Commons approach to design and technology development, a zero-emission future, sustainable urban economic growth, and other topics related to building sustainable, inclusive and resilient cities.
<br>
<br>
*Technologies*
<br>
<br>
• 3D printing
<br>
<br>
The first technology for 3D printing was developed in the 80s, in the industrial context. Since then, technology evolved and when in the 2000s, patents expired, the development of machines in the context of the Maker, DIY and Hacker movements started to happen. Printers were made in an open source context, where people could build their own or sell for an affordable price. The advantage of 3D printing over other digital fabrication processes is that it is additive manufacturing - it adds material instead of subtracting from a raw source, allowing for minimum waste. The current struggle is to overcome the usage of plastic, as it is the most common material and brings many issues. This advance is coming in the development of new materials that can be biodegradable and restored to the natural system, as well as being possible to be 3D printed.
<br>
<br>
• New materials
<br>
<br>
The development of new materials is an imperative in the current situation, in which materials are extracted from nature, processed and after usage (sometimes of single use) are considered waste. Many designers have started to explore with materials that are discarded by the industry or people, adding value to things previously seen as unvalued. As it is important to reshape the things from our current system, it is essential that more regenerative processes start to flourish. Explorations with biodegradable materials, bacteria and mycelium are already proving to bring an alternative to many materials we have been using and are unsustainable. However, it is still a field to be developed for proving the needed applicability into the world of things.
<br>
<br>
{% figure caption: "*Material based on avocado seed developed in the Material Driven Design class*" %}
![]({{site.baseurl}}/p_t03_material.jpg)
{% endfigure %}
<br>
<br>
*Societal changes*
<br>
<br>
• Work relations
<br>
<br>
As more and more technology is being developed towards the automation of jobs and the application of machine processing, human’s relationship with the concept of work needs to be reframed. Low-skilled jobs will be most impacted and people in these positions are the less educated ones. Education is a key factor in giving people the power of decision about their futures and moreover, to allow people to think and form opinions. Ironically, the presence of technology in our lives is decreasing the ability of forming opinions, making decisions and giving tolerance to cohabit with other’s opinions. In a future where many jobs of repetitive tasks will be done by machines, what will the humans be doing? Works related to creativity and care will be fundamental, they express what is inherently human, dealing with feelings and emotions and later demonstrating them. Expressing identities, cultures and connections with others in a community will become key in reframing what it means to be human.
<br>
<br>
• Mass migration
<br>
<br>
Climate change and political instabilities and conflicts are impacting many parts of the world, causing people to migrate to wealthier regions. Different cultures crash and also the proportion of migration creates a reaction from the local population, in many ways giving rise to feelings of fear, intolerance and even hate. In this scenario, people that are coming from afar, hardly are able to establish a real connection to that city and its community. The projections for the next decades are that these territorial conflicts will only rise. As the most unprivileged people are migrating, there is also the other extreme of the situation, in which the most privileged people are allowed to work remotely. Not developing a relationship to the place where you live by the dynamics of work, can also impact in a disconnection to that place. So how people that are constantly moving or being kept apart from a community can develop a sense of care for that land? In this way, the flourishing of relationships with people and place through making can be a tool for real connections.
<br>
<br>
<br>
<br>
By introducing the concepts, technologies and societal changes that are considered for this project it is possible to justify decisions and estimate its futures’ possibilities. By choosing to analyse the making in the urban context, as explained before, the relevance of the cities as places for living is undeniable. For cities’ futures scenarios it is imperative that they start to reframe the relations of sourcing materials, producing, using and disposing. The intervention of this project tries to shed light on the reframing of these processes. As for the chosen technique, 3D printing makes evident that additive manufacturing will gain more importance, even allowing the exploration with new kinds of materials. A future iteration for this project could be to print with new kind of materials, as the one developed during the Material-Driven Design class. This was a subject we had in the master in which we developed biodegradable materials out of sources considered waste. In futures’ scenarios, it is possible to envision that each community will explore the potentialities of its own culture and available resources to develop products and services best tailored to their specificities. However, communities will need to learn to embrace the difference, the ones coming from outside, as more and more people will be displaced. Also, immigrants need to choose if they will belong, if they want to identify with that new reality in order to engage with the community. In sharing a collective effort of making with their Commons knowledge, people from the outside can enrich to a communal ground for exchange of experiences.
<br>
<br>
These are the beliefs that the intervention is trying to bring - for the present moment and for the futures. To connect people with material and place, with a view over local resources, enabling diversity and integration.
