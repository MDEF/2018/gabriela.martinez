---
title: 03 • Design for the Real Digital World
period: 15-19 October 2018
date: 2018-10-21 12:00:00
term: 1
published: true
---

*Design for the real digital world* (15-19.10.2018) proposed to us the creation of new furniture and objects with the material collected from the streets during the MDEF Bootcamp week. We were able to repurpose these discarded materials into new products to make our studio space more personalized.
<br>
<br>
### Conceptualization and Design
<br>
The week started with new groups for design development. The task, beyond creating furniture and objects, was to reconsider the actual layout of the class and the existing furniture. The discussion in our group started with finding out the deficiencies we believed the room had: lack of ventilation, lack of lighting and complex spatial context due to two columns in the middle of the room. We then started discussing how we could change the layout of the tables for the lectures, so the columns would not be an obstacle. We thought of modular configurations that could group and separate due to demands, thought about add-ons to tables to change their shapes or utilities. The process of design discussion can be a bit hard at first with people you recently met and come from different backgrounds. We tried to move on with the discussion doing studies with physical models and on the computer over the floor plan. We were having trouble into reaching some conclusion about the tables when we realized that we should develop other new furniture to increment the space.
<br>
<br>
{% figure caption: "*Studies around changes for tables*" %}
![]({{site.baseurl}}/w03_01.jpg)
{% endfigure %}
<br>
<br>
The new focus was into developing a shelf for storage of belongings and inventory (of materials and projects of the course) and a coffee lab that could take advantage from the coffee waste as a nutrition to soil for mushroom growth, with humidity control by sensors connected to an Arduino.
<br>
<br>
We split the group in two so we could develop both projects at the same time. Along with Julia and Silvia, I was working on the development of the shelf. We first looked out for the material available and got inspiration from one of the structures collected from the streets to develop the structure of the shelf. However, after talking with the other half of the group and seeing the development of the coffee lab we realized that it would be nice to have the same aesthetics language to both. Therefore, we incorporated organic shapes to the shelf as the coffee lab had many layers of sinuous curves.
<br>
<br>
![]({{site.baseurl}}/w03_02.jpg)
<br>
{% figure caption: "*First models for coffee lab and shelf*" %}
![]({{site.baseurl}}/w03_03.jpg)
{% endfigure %}
<br>
<br>
### Presentation and Digital Fabrication
<br>
All the groups presented their ideas and one or two pieces from each group were selected to be produced. We then started working on the production of the coffee lab. Part of the group started to research about mushroom cultivation and another part started to work on the materials and final model for production. Me and Nicolás started to work on the production drawings for the table, refining the 3D model and developing the joinery of pieces. Since from the last two years I have been working on developing furniture and bespoke projects with digital fabrication (mostly CNC milling), this was a moment that I could share part of my knowledge of the process with the colleagues. I could explain them about the need of leaving a gap of tolerance and the insertion of “bones” so the pieces could fit perfectly. Moreover, explained how we make the files ready, making a 2D visualization of the pieces and studying the nesting of each board we would cut. It was an interesting moment where I realized that I could share information and communicate it in a way that people could understand the process.
<br>
<br>
{% figure caption: "*Drawings for production and CNC milling process*" %}
![]({{site.baseurl}}/w03_04.jpg)
{% endfigure %}
<br>
<br>
With help from the Fab Lab team, we made the files ready to be sent for the machine to cut. We generated the G-codes with the RhinoCAM plugin. This was new information for me since I usually do not use Rhino on my studio’s projects. Therefore, it was interesting to learn that on the same software we can develop the product and generate the G-codes. With the files ready, the Fab Lab team explained the procedures to use the CNC machines and we started to cut the pieces for the table. At that point, the main structure of the table was already assembled. This structure was made by solid wood found on the streets and cut and joined by carpentry work. The boards found on the streets were used to be cut on the CNC and other collected materials were used for the mushroom growth box. A box to protect the Arduino was cut on the laser machine and Ilja worked on the electronics for the sensors to work.
<br>
<br>
{% figure caption: "*CNC milling instructions with Fab Lab team*" %}
![]({{site.baseurl}}/w03_05.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Sanding of the pieces for the coffee lab*" %}
![]({{site.baseurl}}/w03_06.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Container for mushroom cultivation and Arduino box*" %}
![]({{site.baseurl}}/w03_07.jpg)
{% endfigure %}
<br>
<br>
By Friday morning, we finished the last details on the coffee lab. We sanded the pieces for a better fit and joined the parts for making the shelves. Also, the other part of the team finished the box for the mushrooms and the connections for the Arduino. By the afternoon, each group presented the pieces they produced. It was interesting to see what was the driving force for each group to develop its own project. One of the groups started by big parts of what was found on the streets, like a whole coffee table, doors and drawers. With these pieces in hand, they created new pieces that looked hybrid furniture. Another group developed interventions on the space to make it more interactive, creating message boards, mirrors, a relaxing space and also modules that can serve as side tables to take notes or laptop support. The other group worked on a multi-task station to serve as a mini fab lab, so we can develop work without leaving the studio. Our group was driven to think besides the reuse of materials from the streets, we could use our own waste from drinking coffee to nurture a cultivation. Moreover, on this cultivation, technology would be an ally, informing us about the state of the soil and different phases of growth of the mushrooms through a sensor connected to Arduino. For the ones not drinking coffee, herbs will provide material for tea making. These other elements are embraced by holes on the table that dialogue with the curves of the surfaces.
<br>
<br>
{% figure caption: "*Final result of the coffee lab*" %}
![]({{site.baseurl}}/w03_08.jpg)
{% endfigure %}
<br>
<br>
### Conclusions
<br>
This week of design and fabrication was an intense week of group work, posing a challenge in making collective decisions and producing a consistent piece on time.  I felt I could contribute in sharing my knowledge of digital fabrication with my colleagues and also could work my patience in the pace of time for setting the drawings. Since it is something I already have a practice on working, I know I could do the drawings more quickly. However, as it was a teamwork and colleagues were learning from zero, I worked to control my anxiety of drawing faster and discovered enjoyment in being able to explain details of production. Moreover, learning about RhinoCAM and specific practices and ways of work from the Fab Lab team was really valuable.
<br>
<br>
The change of the space of the studio was notable and it is possible to feel a better identity with it now. Certainly, this was a week to integrate more the group in knowledge exchange and collaboration in teamwork. We had the chance to learn specificities of the digital fabrication for product design by doing the pieces and working with the machines. Another highlight was the possibility of doing products from materials found locally and freely from the streets of Poblenou, giving us a sense of the potentialities of the circular economy.
