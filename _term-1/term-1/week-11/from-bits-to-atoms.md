---
title: 11 • From Bits to Atoms
period: 10-14 December 2018
date: 2018-12-16 12:00:00
term: 1
published: true
---

<br>
<br>
From Bits to Atoms (10-14.12.2018) gave us insight in what we will be doing next trimesters during the Fab Academy. The objective of the week was to combine a design process with the development of interactions through electronics. It was presented to us in more detail the tools for digital fabrication: laser cutter, CNC milling and 3D printing.
<br>
<br>
In groups, we had to develop a project of a machine that would combine design and electronics. My group was interested in developing a machine related to weaving, and so, after a brainstorm, we decided to make a machine to do pom poms. We named the machine as *DJ Pom Pom*. The process was all documented on a dedicated website for it, which can be consulted at:
<br>
<br>
[https://mdef.gitlab.io/dj-pom-pom/](https://mdef.gitlab.io/dj-pom-pom/)
<br>
<br>
{% figure caption: "*DJ Pom Pom, a machine to make pom poms*" %}
![]({{site.baseurl}}/w11_01.jpg)
{% endfigure %}
<br>
<br>
Here I will comment about my participation in the project. After discussing in group how the design of the machine would be, my first task was to model this design, a box which would contain all the electronic parts. I did a 3D model first, then extracted the top view of the pieces as a 2D drawing. The file was saved as .dwf extension. The box was later customized by Ola and Vicky, who made some changes in the design and 3D printed buttons for it.
<br>
<br>
After working on this part of the design, I joined Barbara in the exploration of the electronics and code to make the machine work. With the help of the tutors, we could gather all the parts needed and started setting the electronics for it. Internet search was a valuable tool for the first arrangement of cables in the Arduino and breadboard. The tutors helped us rearrange it for the best working of the machine. Refining the electronics and code was a laborious task since the stepper motor was sensitive to vibrations. The help of the tutors was really valuable and we were happy to find the best setting so we could move forward and set up the box.
<br>
<br>
The set-up of the box was done by the whole group, with each of us giving opinions and insights to arrange it in the most effective way. The interaction with other external elements, for example the wool, were needed to be taken into account in this process. Different heights for these elements (wool and support sticks) were tested to find the best way of operation. The final result of the machine can be seen in the video below, as we show the whole process of making a pom pom.
<br>
<br>
<iframe width="672" height="378" src="https://www.youtube.com/embed/A1WgZpz1pbY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
