---
title: 10 • An Introduction to the Future
period: 03-07 December 2018
date: 2018-12-09 12:00:00
term: 1
published: true
---

<br>
<br>
*An Introduction to the Future* (03-07.12.2018) was a week with Elisabet Roselló Román as a tutor. Roselló is a strategic innovation consultant and fellow of CPPFS (The Centre for Postnormal Policy and Futures Studies).
<br>
<br>
The first session consisted of ‘Unthinking the future’. First, she presented us the main concepts that compound the idea of future: visions, narratives and mental structures. Different visions of future were discussed: dystopic, no future, apocalyptical, post-apocalyptical, transhumanism and post humanism. Moreover, Roselló presented us the concept of the future cone, in which a horizon of possibilities exists and futurists try to guess through different scenarios how future can be. Different concepts were further discussed, as liquid modernity, self-fulfil prophecy and black swans.  Movements that try to envision futures as Afrofuturism, Gulf futurism and queer perspectives were also commented.
<br>
<br>
After, we did an exercise to create a future scenario based on two axis. My group had the city-state axis and the feminist axis. Our future would be of gender full equality and city-states. We discussed about many ideas that could compound this scenario, that we called *The Universal Society*. People would have shared houses and tasks in this society. Everybody would be requested on voting. On work, people would rotate from their regular tasks into working at the nursery, restaurant and other organizational functions. A universal key would give access to all services for the citizens, like free public transport. It would also contain data and other information to guide people. This key would also register transactions at the market and people would go to it regularly to increase social contact. Communal events like public dinners and movie projections would be a regular thing in this society’s life.
<br>
<br>
{% figure caption: "*Speculating a future scenario*" %}
![]({{site.baseurl}}/w10_01.jpg)
{% endfigure %}
<br>
<br>
The next class was about the foundations of future thinking. The concept of future was discussed in the framework of different civilizations, as well as the concept of utopias. Some visions of possible futures were also discussed: Postfuturism, Retrofuturism, Transhumanism and Posthumanism.
<br>
<br>
We were introduced to different approaches of envisioning futures, from classic futurists and their different laws and diagrams to other strategies like narratives in design fiction and speculative design. Roselló discussed the concept of change, to later introduce a reflection on trends. We saw how to categorize a trend by shape, size and function; also how it evolves through time, from being emergent, having growth, stagnation or an end. Also, some megatrends were presented: glocality, state-nations crises, post-truth era, postdigitalism, post-gender movements, and many more.
<br>
<br>
The next session we had an invited tutor, Jordi Serra del Pino, from the *Center for Postnormal Policy and Future Studies*. A methodology to research possible futures was presented to us. *The future as 3 tomorrows* approaches the possibilities by envisioning a first tomorrow that is a vision of an extended present, a second tomorrow as a familiar future and a third tomorrow as an unthought future. By navigating from a first scenario more close to our realities, then moving to one with novelty but yet familiarity to after move to a scenario that bring concepts out of our worldview, it is possible to have a perspective of all spectrums and create diverse possibilities of a potential future.
<br>
<br>
In the final session with Roselló, we were presented with strategies to research the future. Speculative design futures research comprehend projecting scenarios through a storytelling that can be tested with interaction, providing feedback. Ethnographic and experiential futures research is a process of gathering data and perceptions of different scenarios. Casual layered analysis is an exercise to deconstruct a future scenario.
<br>
<br>
To conclude the week, we would work around one topic and project different consequences if that thing would happen in the world. Our group had to analyze the possible implications of a rise in social inequality. A first round of consequences than should be break into another round of consequences. We would have then sub-consequences from the ones previous thoughts.  As a final result, we had:
<br>
<br>
With the rise of social inequity…
<br>
<br>
• Lack of access in education and health care: illiteracy, lack of access to information, language barrier (isolation), health at home
<br>
<br>
• Centralization of power: individualism, different rules for different people, corruption, rich people protecting rich people
<br>
<br>
• Lack of access to basic infrastructure: rise of slums, diseases, food access
<br>
<br>
• Lack of Social mobility (it becomes harder ascend): modern slavery, unemployment, workers with no rights (e.g. Uber, Deliveroo)
<br>
<br>
• Technological Division: disposable poor people, eugenics, lifespan, lack of organization
<br>
<br>
• Civic Rights: not able to vote, civil unrest
<br>
<br>
### Conclusions
<br>
<br>
This week gave us reflections and tools to understand how the idea of future is shaped, both collectively and individually. Moreover, we learned different approaches to research future scenarios and test it through design tools. It was interesting to see a wide range of topics that orbit our societal interest in projecting life in a future moment and I hope that society awakes to the need of a shift in perspective, making it less human centered and more resilient to our inhabitancy in planet Earth.
